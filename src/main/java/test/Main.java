package test;

import akka.actor.*;
import scala.concurrent.duration.Duration;
import scala.util.Try;

import java.io.Serializable;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

public final class Main {

    public static void main(String[] args) throws Exception {

        ActorSystem system = ActorSystem.create("helloakka");

        ActorRef greeter = system.actorOf(Props.create(Greeter.class), "greeter");
        Inbox inbox = Inbox.create(system);

        greeter.tell(new WhoToGreet("akka"), ActorRef.noSender());
        inbox.send(greeter, new Greet());

        Greeting greeting1 = (Greeting) inbox.receive(Duration.create(1, TimeUnit.SECONDS));
        System.out.println("Greeting: " + greeting1.message);

        greeter.tell(new WhoToGreet("typesafe"), ActorRef.noSender());
        inbox.send(greeter, new Greet());

        Greeting greeting2 = (Greeting) inbox.receive(Duration.create(1, TimeUnit.SECONDS));
        System.out.println("Greeting: " + greeting2.message);

        system.terminate();
    }

    public static class Greet implements Serializable {
    }

    public static class WhoToGreet implements Serializable {

        public final String who;

        public WhoToGreet(String who) {
            this.who = who;
        }
    }

    public static class Greeting implements Serializable {
        private final String message;

        public Greeting(String message) {
            this.message = message;
        }
    }

    public static class Greeter extends UntypedActor {

        private String greeting = "";

        @Override
        public void onReceive(Object message) {
            if (message instanceof WhoToGreet) {
                greeting = "hello, " + ((WhoToGreet) message).who;
            } else if (message instanceof Greet) {
                getSender().tell(new Greeting(greeting), getSelf());
            } else {
                unhandled(message);
            }
        }

    }

}